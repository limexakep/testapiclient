package com.example.ms1.client.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}