package com.example.ms1;

import com.example.ms1.client.api.*;
import com.example.ms1.client.invoker.*;
import org.springframework.context.annotation.*;
import org.springframework.web.client.*;

@Configuration
public class ResTemplateConfiguration {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public CompanyControllerApi companyControllerApi(ApiClient apiClient) {
        apiClient.setBasePath("http://localhost:8082/");
        return new CompanyControllerApi(apiClient);
    }

}
