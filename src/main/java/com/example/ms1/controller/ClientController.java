package com.example.ms1.controller;

//import com.netflix.discovery.*;
import com.example.ms1.client.api.*;
import com.example.ms1.client.invoker.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/client")
@RequiredArgsConstructor
public class ClientController {
    //private final EurekaClient eurekaClient;

    private final CompanyControllerApi companyControllerApi;

    @GetMapping("/{name}")
    public ResponseEntity<String> sayHelloClient(@PathVariable String name) {
        return ResponseEntity.ok(companyControllerApi.sayHelloCompany(name));
    }
}
